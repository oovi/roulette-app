# Roulette results application

## Requirements

- Node.js (npm)
- Open _8081_ port (could be changed inside webpack.config.js)

## Setting it up

- Install dependencies via CLI using: `npm i`
- Start project via CLI using: `npm run dev`
- Build a production code using: `npm run build`
