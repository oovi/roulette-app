const path = require('path');
const webpack = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');

module.exports = () => ({
  entry: './src/app.js',
  resolve: {
    modules: [path.resolve(__dirname, 'src'), 'node_modules'],
  },
  devtool: 'source-map',
  devServer: {
    contentBase: [path.resolve(__dirname, './')],
  },
  module: {
    rules: [
      {
        test: /\.(css|scss)$/,
        use: [
          'style-loader', // creates style nodes from JS strings
          'css-loader', // translates CSS into CommonJS
          'postcss-loader', // injects postcss tools like autoprefixer in postcss.config.js
          'resolve-url-loader',
          {
            loader: 'sass-loader',
            options: {
              includePaths: ['./src/'],
            },
          }, // compiles Sass to CSS, Sets initial value
        ],
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
    ],
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '',
    chunkFilename: `./[name].chunk.js`,
    filename: `./index.js`,
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: './src/index.html',
    }),
    new webpack.DefinePlugin({
      API_URL: JSON.stringify(require('./package.json').config.apiURL),
    }),
  ],
});
