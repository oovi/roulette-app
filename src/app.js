import React, { useEffect, useState } from 'react';
import format from 'date-fns/format';
import ReactDOM from 'react-dom';
import handleFetch from 'functions/handleFetch';
import Box from 'containers/Box';
import CountDown from 'components/CountDown';
import EventLog from 'components/EventLog';
import 'app.scss';
import GameBoard from 'components/GameBoard';
import StatsLog from 'components/StatsLog';

const App = () => {
  const [configuration, setConfiguration] = useState(false);
  const [gameLog, setGameLog] = useState([]);
  const [nextGame, setNextGame] = useState(false);
  const [stats, setStats] = useState(false);

  const logData = (entry, data = gameLog, setData = setGameLog) => {
    const date = format(new Date(), 'YYYY/MM/DD HH:mm:ss');
    const logText = `[${date}] ${entry}`;
    setData([...data, logText]);
    return logText;
  };

  const initialDataLoad = (errorData = []) => {
    handleFetch({
      configuration: `${API_URL}/1/configuration`,
      nextGame: `${API_URL}/1/nextGame`,
      stats: `${API_URL}/1/stats?limit=200`,
    })
      .then(response => {
        logData('Game board, stats and next game loaded', errorData),
          setConfiguration(response.configuration);
        setNextGame(response.nextGame);
        setStats(response.stats);
      })
      .catch(() => {
        const newErrorData = [
          ...errorData,
          logData('Failed to load initial data, retrying..', errorData),
        ];
        setTimeout(() => {
          initialDataLoad(newErrorData);
        }, 1000);
      });
  };

  useEffect(() => {
    initialDataLoad();
  }, []);

  return (
    <>
      {stats && (
        <Box title="Stats (last 200 games)" id="stats">
          <StatsLog configuration={configuration} stats={stats} />
        </Box>
      )}
      {nextGame && (
        <Box title="Events" id="panel1">
          <CountDown
            logDataFN={logData}
            gameLog={gameLog}
            setStatsFN={setStats}
            setGameLogFN={setGameLog}
            setNextGameFN={setNextGame}
            {...nextGame}
          />
        </Box>
      )}
      {configuration && (
        <Box title="Game board" id="panel2">
          <GameBoard configuration={configuration} />
        </Box>
      )}

      <Box title="Logs:" id="panel3">
        <EventLog logs={gameLog} />
      </Box>
    </>
  );
};

ReactDOM.render(<App />, document.getElementById('app'));
