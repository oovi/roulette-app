import React from 'react';
import './box.scss';
const Box = ({ children, title, id }) => {
  return (
    <div className="Box" style={{ gridArea: id }}>
      {title ? <div className="Box__title">{title}</div> : null}
      <div className="Box__content">{children}</div>
    </div>
  );
};

export default Box;
