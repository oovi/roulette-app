const handleFetch = (URL, key) => {
  // For handling multiple requests
  if (typeof URL === 'object') {
    const URLResponses = Object.keys(URL).map(urlInstance =>
      handleFetch(URL[urlInstance], urlInstance)
    );
    return Promise.all(URLResponses).then(responses => {
      const responseObject = {};
      responses.forEach(
        response => (responseObject[response.key] = response.value)
      );
      return responseObject;
    });
  }
  if (navigator.onLine) {
    return fetch(URL)
      .then(response => response.json())
      .then(response =>
        key
          ? {
              value: response,
              key,
            }
          : response
      );
  } else {
    return Promise.reject({ error: 'offline' });
  }
};
export default handleFetch;
