import React, { useState, useEffect } from 'react';
import differenceInSeconds from 'date-fns/difference_in_seconds';
import './CountDown.scss';
import handleFetch from 'functions/handleFetch';
import useInterval from 'functions/useInterval';

const CountDown = ({
  id,
  startTime,
  setNextGameFN,
  setStatsFN,
  gameLog,
  logDataFN,
}) => {
  const [timeLeft, setTimeLeft] = useState();
  const [isUpdating, setIsUpdating] = useState(false);
  const [gameEvents, setGameEvents] = useState([]);

  const updateTime = () => {
    const timeDifference = differenceInSeconds(new Date(startTime), new Date());
    if (timeDifference <= 0 && !isUpdating) {
      logDataFN('Stopping the wheel', gameLog);
      setTimeLeft('Updating...');
      setIsUpdating(true);
      handleFetch({
        stats: `${API_URL}/1/stats?limit=200`,
        currentGame: `${API_URL}/1/game/${id}`,
        nextGame: `${API_URL}/1/nextGame`,
      })
        .then(results => {
          setIsUpdating(false);
          setStatsFN(results.stats);
          setNextGameFN(results.nextGame);
          if (results.currentGame.result) {
            logDataFN(
              `Game ${results.currentGame.id} ended, result is ${
                results.currentGame.result
              }`,
              gameEvents,
              setGameEvents
            );
            logDataFN(
              `Next game starts in ${results.nextGame.fakeStartDelta} seconds`
            );
          }
        })
        .catch(error => {
          setIsUpdating(false);
          logDataFN(`Failed to fetch: ${error}`);
        });
    } else {
      setTimeLeft(timeDifference);
    }
  };

  useEffect(() => {
    updateTime();
  }, []);

  useInterval(updateTime, 1000);

  return (
    <div className="CountDown">
      <span className="CountDown__text">Game {id} will start in: </span>
      <span className="CountDown__value">{timeLeft}</span>
      <div className="CountDown__container">
        {gameEvents.map(gameEvent => (
          <p className="CountDown__item" key={gameEvent}>
            {gameEvent}
          </p>
        ))}
      </div>
    </div>
  );
};

export default CountDown;
