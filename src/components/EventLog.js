import React from 'react';
import './EventLog.scss';

const EventLog = ({ logs }) => {
  return (
    <div className="EventLog">
      {logs.length ? (
        <div className="EventLog__list-container">
          {logs.map(log => (
            <div className="EventLog__list-entry" key={log}>
              {log}
            </div>
          ))}
        </div>
      ) : (
        <div className="EventLog__message">No data yet...</div>
      )}
    </div>
  );
};

export default EventLog;
