import React from 'react';
import './StatsLog.scss';

const StatsLog = ({ stats, configuration: { colors } }) => {
  return (
    <div className="StatsLog">
      <table className="StatsLog__table">
        <tbody>
          <tr className="StatsLog__table-row">
            <th />
            <th
              colSpan="5"
              className="StatsLog__table-title StatsLog__table-title--cold"
            >
              Cold
            </th>
            <th
              colSpan="27"
              className="StatsLog__table-title StatsLog__table-title--neutral"
            >
              Neutral
            </th>
            <th
              colSpan="5"
              className="StatsLog__table-title StatsLog__table-title--hot"
            >
              Hot
            </th>
          </tr>
          <tr className="StatsLog__table-row">
            <th className="StatsLog__table-title">Slot</th>
            {stats.map((stat, index) => (
              <td
                className={`StatsLog__table-item StatsLog__table-item--${
                  colors[stat.result]
                }`}
                key={index}
              >
                {stat.result}
              </td>
            ))}
          </tr>
          <tr className="StatsLog__table-row">
            <th className="StatsLog__table-title">Hits</th>
            {stats.map((stat, index) => (
              <td
                className="StatsLog__table-item StatsLog__table-item--value"
                key={index}
              >
                {stat.count}
              </td>
            ))}
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default StatsLog;
