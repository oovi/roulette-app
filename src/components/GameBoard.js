import React from 'react';
import './GameBoard.scss';

const GameBoard = ({ configuration }) => {
  return (
    <div className="GameBoard">
      <div className="GameBoard__container">
        {configuration.positionToId.map(result => (
          <div
            key={result}
            className={`GameBoard__item GameBoard__item--${
              configuration.colors[result]
            }`}
          >
            {result}
          </div>
        ))}
      </div>
    </div>
  );
};

export default GameBoard;
